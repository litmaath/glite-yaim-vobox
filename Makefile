prefix=/opt/glite
package=glite-yaim-vobox
version=1.0.3
release=1

.PHONY: configure install clean rpm

all: configure

install: 
	@echo installing ...
	@mkdir -p $(prefix)/yaim/examples/siteinfo
	@mkdir -p $(prefix)/yaim/bin
	@mkdir -p $(prefix)/yaim/etc
	@mkdir -p $(prefix)/yaim/log

	@install -m 0644 README $(prefix)/yaim/README

	@install -m 0644 examples/groups.conf $(prefix)/yaim/examples
	@install -m 0644 examples/groups.conf.README $(prefix)/yaim/examples
	@install -m 0644 examples/users.conf $(prefix)/yaim/examples
	@install -m 0644 examples/users.conf.README $(prefix)/yaim/examples
	@install -m 0644 examples/siteinfo/site-info.def $(prefix)/yaim/examples/siteinfo/

	@install -m 0755 src/bin/yaim $(prefix)/yaim/bin

clean::
	rm -rf rpmbuild 

rpm:
	@mkdir -p  RPMS
	@mkdir -p  rpmbuild/RPMS/noarch
	@mkdir -p  rpmbuild/SRPMS/
	@mkdir -p  rpmbuild/SPECS/
	@mkdir -p  rpmbuild/SOURCES/
	@mkdir -p  rpmbuild/BUILD/

	@sed -i 's/^Version:.*/Version: $(version)/' $(package).spec
	@sed -i 's/^Release:.*/Release: $(release)%dist/' $(package).spec

	@tar --gzip --exclude='rpmbuild*' --exclude='RPMS' -cf rpmbuild/SOURCES/${package}.src.tgz *
	rpmbuild --define 'topdir %(pwd)/rpmbuild' --define '_topdir %{topdir}' -ba ${package}.spec
	cp rpmbuild/RPMS/noarch/*.rpm rpmbuild/SRPMS/*.rpm RPMS/.

