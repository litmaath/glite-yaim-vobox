Summary: simple configuration script for a WLCG VObox
Name: glite-yaim-vobox
Version: 1.0.3
Vendor: WLCG
Release: 1%dist
License: ASL 2.0
Url: https://gitlab.cern.ch/litmaath/glite-yaim-vobox
Group: System Environment/Admin Tools
Source: %{name}.src.tgz
BuildArch: noarch
Prefix: /opt/glite
BuildRoot: %{_tmppath}/%{name}-%{version}-build
Packager: CERN
Requires: chkconfig

%description
This package contains a simple configuration script for a WLCG VObox.

%prep

%setup -c

%install
make install prefix=%{buildroot}%{prefix}

%files
%defattr(-,root,root)
%dir %{prefix}/yaim/etc
%dir %{prefix}/yaim/log
%{prefix}/yaim/README
%{prefix}/yaim/bin/yaim
%{prefix}/yaim/examples/groups.conf
%{prefix}/yaim/examples/groups.conf.README
%{prefix}/yaim/examples/siteinfo/site-info.def
%{prefix}/yaim/examples/users.conf
%{prefix}/yaim/examples/users.conf.README

%clean
rm -rf %{buildroot}

